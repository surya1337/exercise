const mongoose = require("mongoose");
const customers = require("./routes/customers");
const express = require("express");
const app = express();

mongoose
  .connect("mongodb://localhost/vidly")
  .then(() => {
    console.log("Connected to DB");
  })
  .catch((err) => {
    console.error("Not Connected", err);
  });

app.use("/api/customers", customers);
app.use(express.json());

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`listening from ${port}`));
